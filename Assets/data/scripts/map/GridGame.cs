﻿using System.Collections.Generic;
using UnityEngine;

namespace RedDev.Gameplay.Map
{
    /// <summary>
    /// Объект описывает игровое поле.
    /// </summary>
    public class GridGame : MonoBehaviour
    {
        /// <summary>
        /// Словарь с ключём - позицией клетки, и значением непосредственно самой клеткой.
        /// </summary>
        public Dictionary<Vector3, Cell> Cells = new Dictionary<Vector3, Cell>();

        void Awake()
        {
            Cells.Clear();
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform tmp = transform.GetChild(i);
                if (tmp.tag == "cell")
                {
                    Cell cell = tmp.GetComponent<Cell>();
                    if (cell == null)
                        Debug.LogError("На ячейке " + tmp.name + " отсутствует компанент Cell.");
                    else
                        Cells.Add(cell.transform.position, cell);
                }
            }
        }
    }
}