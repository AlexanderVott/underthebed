﻿using UnityEngine;

namespace RedDev.Gameplay.Map
{
    /// <summary>
    /// Класс описывает клетку игрового поля.
    /// </summary>
    public class Cell : MonoBehaviour
    {
        /// <summary>
        /// Физический размер ячейки.
        /// </summary>
        [Tooltip("Физический размер ячейки")]
        public Vector2 Size;

        /// <summary>
        /// Указывает на то, что это непроходимая, заблокированная ячейка для реализации «стен».
        /// </summary>
        [Tooltip("Указывает на то, что это непроходимая ячейка для реализации «стен»")]
        public bool Blocked;
    }
}