﻿using System.Collections;
using RedDev.Gameplay.Game;
using UnityEngine;
using UnityEngine.UI;

namespace RedDev.Gameplay.UI
{
    public class StateSessionUI : MonoBehaviour
    {
        private Image Panel;
        private Text _text;

        private float _maxAlphaPanel;
        private bool _showingPanel = false;

        [Tooltip("Скорость появления и исчезания подсказки")]
        public float SpeedShowing = 2.0f;

        [Tooltip("Время ожидания до того, как спрячется подсказка")]
        public float WaitForHide = 2.0f;

        [Tooltip("Время ожидания после появления подсказки до её деактивации")]
        public float WaitForDisable = 2.0f;

        /// <summary>
        /// Ссылка на игровую сессию, если при старте не задана осуществляется попытка найти программно.
        /// </summary>
        public GameSession Session;

        void Start()
        {
            Panel = GetComponent<Image>();
            _text = GetComponentInChildren<Text>();

            // TODO: лучше бы переписать подобную конструкцию.
            Color tmpColor = Panel.color;
            _maxAlphaPanel = tmpColor.a;
            tmpColor.a = 0.0f;
            Panel.color = tmpColor;

            tmpColor = _text.color;
            tmpColor.a = 0.0f;
            _text.color = tmpColor;

            if (Session == null)
                Session = Transform.FindObjectOfType<GameSession>();

            if (Session == null)
                Debug.LogError("Не назначен объект игровой сессии для " + transform.name);
            else
            {
                Session.onGameHintState += hint =>
                {
                    StopAllCoroutines();
                    gameObject.SetActive(true);
                    _text.text = hint;
                    StartCoroutine(ShowHint());
                };
            }
        }

        void Update()
        {
            Color tmpColor = Panel.color;
            tmpColor.a = _showingPanel ? _maxAlphaPanel : 0.0f;
            Panel.color = Color.Lerp(Panel.color, tmpColor, Time.deltaTime * SpeedShowing);
            tmpColor = _text.color;
            tmpColor.a = _showingPanel ? 1.0f : 0.0f;
            _text.color = Color.Lerp(_text.color, tmpColor, Time.deltaTime * SpeedShowing * 2.0f);
        }

        private IEnumerator ShowHint()
        {
            _showingPanel = true;
            yield return new WaitForSeconds(WaitForHide);
            StartCoroutine(HideHint());
        }

        private IEnumerator HideHint()
        {
            _showingPanel = false;
            yield return new WaitForSeconds(WaitForHide);
            gameObject.SetActive(false);
        }
    }
}