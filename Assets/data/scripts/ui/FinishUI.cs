﻿using System;
using System.Collections;
using RedDev.Gameplay.Game;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RedDev.Gameplay.UI
{
    public class FinishUI : MonoBehaviour
    {
        private Image _panel;
        private Image _img;
        private Button _btn;
        private Image _btnImage;
        private Text _btnText;

        [Tooltip("Скорость появления и исчезания основного меню")]
        public float SpeedShowing = 3.0f;

        [Tooltip("Основной логотип игры для отображения в меню паузы")]
        public Sprite Logo;

        [Tooltip("Изображение отображаемое в случае победы")]
        public Sprite Win;

        [Tooltip("Изображения отображаемое в случае проигрыша")]
        public Sprite Failure;

        [Tooltip("Изображение индикатора загрузки")]
        public Sprite Loading;

        [Tooltip("Объект игровой сессии")]
        public GameSession Session;

        private float _maxAlphaPanel;
        private bool _showingPanel = false;

        private EStateSession _currentState = EStateSession.Pause;
        private EStateSession _oldState = EStateSession.Player;

        void Start()
        {
            Color tmpColor;
            // получаем фоновую панель
            // HACK: если будут другие потомки с подобными компонентами могут возникнуть проблемы. Реализация только для конкретного тестового задания.
            _panel = GetComponent<Image>();
            if (_panel == null)
                Debug.LogError("Отсутствует компонент Image для основной панели меню " + transform.name);
            else
            {
                tmpColor = _panel.color;
                _maxAlphaPanel = tmpColor.a;
                tmpColor.a = 0.0f;
                _panel.color = tmpColor;
            }

            // основная надпись меню
            _img = transform.Find("Image").GetComponent<Image>();
            if (_img == null)
                Debug.LogError("Необнаружен объект Image для основной надписи меню " + transform.name);
            else
                _img.color = new Color(_img.color.r, _img.color.g, _img.color.b, 0.0f);

            // кнопка перехода к другому уровню
            _btn = GetComponentInChildren<Button>();
            if (_btn == null)
                Debug.LogError("Необнаружена кнопка основной панели меню " + transform.name);
            else
            {
                // картинка кнопки
                _btnImage = _btn.GetComponent<Image>();
                if (_btnImage == null)
                    Debug.LogError("Отсутствует компонент изображения кнопки основного меню " + _btn.transform.name);
                else
                    _btnImage.color = new Color(_btnImage.color.r, _btnImage.color.g, _btnImage.color.b, 0.0f);
            }

            // текст кнопки перехода к другому уровню
            _btnText = _btn.GetComponentInChildren<Text>();
            if (_btnText == null)
                Debug.LogError("Необнаружен текст кнопки основной панели меню " + transform.name);
            else
                _btnText.color = new Color(_btnText.color.r, _btnText.color.g, _btnText.color.b, 0.0f);

            // проверяем связь с игровой сессией и регистрируем события
            if (Session == null)
                Debug.LogError("Не обнаружен объект GameSession в " + transform.name);
            else
            {
                _btn.onClick.AddListener(new UnityAction(() =>
                {
                    switch (_currentState)
                    {
                        case EStateSession.Pause:
                            if (Session != null)
                            {
                                Session.StateSession = _oldState;
                                StartCoroutine(HideMenu());
                            }
                            break;
                        case EStateSession.Finish:
                            string path = Session.GetNextLevel();
                            SceneManager.LoadScene(!String.IsNullOrEmpty(path)
                                ? path
                                : SceneManager.GetActiveScene().path);
                            break;
                        case EStateSession.Failure:
                            SceneManager.LoadScene(SceneManager.GetActiveScene().path);
                            break;
                        default:
                            break;
                    }
                }));

                Session.onChangeStateSession += (newState, oldState) =>
                {
                    _currentState = newState;
                    _oldState = oldState;
                    switch (newState)
                    {
                        case EStateSession.Pause:
                            _btnText.text = "продолжить";
                            ShowMenu(Logo);
                            break;
                        case EStateSession.Finish:
                            _btnText.text = !String.IsNullOrEmpty(Session.GetNextLevel()) ? "следующий" : "повторить";
                            ShowMenu(Win);
                            break;
                        case EStateSession.Failure:
                            _btnText.text = "заново";
                            ShowMenu(Failure);
                            break;
                        default:
                            break;
                    }
                };
            }
        }

        void Update()
        {
            Color tmpColor = _panel.color;
            tmpColor.a = _showingPanel ? _maxAlphaPanel : 0.0f;
            _panel.color = Color.Lerp(_panel.color, tmpColor, Time.deltaTime * SpeedShowing);
            tmpColor = _btnImage.color;
            tmpColor.a = _showingPanel ? 1.0f : 0.0f;
            _btnImage.color = Color.Lerp(_btnImage.color, tmpColor, Time.deltaTime * SpeedShowing * 2.0f);
            tmpColor = _img.color;
            tmpColor.a = _showingPanel ? 1.0f : 0.0f;
            _img.color = Color.Lerp(_img.color, tmpColor, Time.deltaTime * SpeedShowing * 2.0f);
            tmpColor = _btnText.color;
            tmpColor.a = _showingPanel ? 1.0f : 0.0f;
            _btnText.color = Color.Lerp(_btnText.color, tmpColor, Time.deltaTime * SpeedShowing * 2.0f);
        }

        /// <summary>
        /// Отображает основную панель меню.
        /// </summary>
        /// <param name="sprite"></param>
        private void ShowMenu(Sprite sprite)
        {
            StopAllCoroutines();
            _img.sprite = sprite;
            gameObject.SetActive(true);
            _showingPanel = true;
        }

        /// <summary>
        /// Скрывает основную панель меню.
        /// </summary>
        private IEnumerator HideMenu(float waitTime = 3.0f)
        {
            _showingPanel = false;
            yield return new WaitForSeconds(waitTime);
            gameObject.SetActive(false);
        }
    }
}