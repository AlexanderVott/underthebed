﻿using RedDev.Gameplay.Game;
using UnityEngine;
using UnityEngine.UI;

namespace RedDev.Gameplay.UI
{
    public class HealthUI : MonoBehaviour
    {
        /// <summary>
        /// Ссылка на игровую сессию, если при старте не задана осуществляется попытка найти программно.
        /// </summary>
        public GameSession GameSession;

        private Text _text;

        void Awake()
        {
            _text = transform.GetComponentInChildren<Text>();
            if (_text == null)
                Debug.LogError("Для UI не определён текстовый компонент: " + transform.name);

            if (GameSession == null)
                GameSession = Transform.FindObjectOfType<GameSession>();

            if (GameSession != null)
            {
                if (GameSession.Hero == null)
                    Debug.LogError("Для UI не определён главный персонаж: " + transform.name);
                else
                {
                    GameSession.Hero.onChangeHealth += health => _text.text = health.ToString();
                }
            }
            else
            {
                Debug.LogError("Отсутствует ссылка на объект игровой сессии: " + transform.name);
            }
        }
    }
}