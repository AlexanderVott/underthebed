﻿using System.Collections;
using RedDev.Gameplay.Game;
using RedDev.Gameplay.Map;
using RedDev.Utils;
using UnityEngine;

namespace RedDev.Gameplay.Characters {

    /// <summary>
    /// Тип движения для персонажей.
    /// </summary>
    public enum EMoveType
    {
        /// <summary>
        /// Нет возможности двигаться, нет задачи двигаться.
        /// </summary>
        Not = 0,

        /// <summary>
        /// Простое перемещение.
        /// </summary>
        Move,

        /// <summary>
        /// Атака другого персонажа.
        /// </summary>
        Attack,

        /// <summary>
        /// Передвижение на точку завершения уровня.
        /// </summary>
        Finish
    }

    /// <summary>
    /// Класс описывает персонажей игры.
    /// </summary>
    public class Character : MonoBehaviour
    {
        /// <summary>
        /// Максимальное значения здоровья.
        /// </summary>
        [Tooltip("Максимально возможное здоровье для персонажа.\nДля восстановления здоровья")]
        public int MaxHealth = 5;

        /// <summary>
        /// Уровень здоровья на момент старта уровня.
        /// </summary>
        [SerializeField]
        [Tooltip("Уровень здоровья на момент старта уровня")]
        public int StartHealth = 5;

        private int _health;

        /// <summary>
        /// Текущее здоровье персонажа.
        /// </summary>
        public int Health
        {
            get { return _health; }
            set
            {
                _health = value;
                if (_health <= 0)
                    DoDie();
                if (onChangeHealth != null)
                    onChangeHealth(value);
            }
        }

        /// <summary>
        /// Указывает наступила ли очередь данного персонажа на выполнение действия.
        /// </summary>
        public bool MyQueue { get; set; }

        /// <summary>
        /// Сила атаки персонажа. Число на которое уменьшается здоровье у противника.
        /// </summary>
        [Tooltip("Сила атаки персонажа.\nЧисло на которое уменьшается здоровье у противника")]
        public int AttackForce = 1;

        /// <summary>
        /// Скорость визульного передвижения персонажа.
        /// </summary>
        [Tooltip("Скорость визуального передвижения персонажа")]
        public float SpeedMovement = 10.0f;

        /// <summary>
        /// Делегат описывающий вызов события изменения здоровья персонажа.
        /// </summary>
        /// <param name="health"></param>
        public delegate void ChangeHealth(int health);
        /// <summary>
        /// Событие вызывается когда происходит изменение здоровья.
        /// </summary>
        public event ChangeHealth onChangeHealth;

        /// <summary>
        /// Определяет направление перемещения персонажа
        /// </summary>
        public ESwipeDirection DirectionMovement { get; set; }

        /// <summary>
        /// Ссылка на объект игровой сессии.
        /// </summary>
        public GameSession GameSession { get; set; }

        /// <summary>
        /// Родитель в структуре уровня для персонажа. Текущая родительская клетка.
        /// </summary>
        protected Cell _parent;

        /// <summary>
        /// Класс управляющий игровым полем.
        /// </summary>
        protected GridGame _grid;

        /// <summary>
        /// Отвечает за взаимодействие с анимацией.
        /// </summary>
        private Animator _animator;

        /// <summary>
        /// Характеризует смерть персонажа.
        /// </summary>
        protected bool _dead;

        /// <summary>
        /// Хранит объект ячейки, в которой погиб персонаж.
        /// </summary>
        protected Transform _deadParent;

        /// <summary>
        /// Рендер спрайтов.
        /// </summary>
        private SpriteRenderer _spriteRender;

        void Awake()
        {
            // делаем автоматические связки и осуществляем проверку на корректность этих связей.
            GetParentCell();
            _grid = Transform.FindObjectOfType<GridGame>();
            if (_grid == null)
                Debug.LogError("Не найден объект управления ячейками");
            else
            {
                GameSession = _grid.GetComponent<GameSession>();
                if (GameSession == null)
                    Debug.LogError("Не найден объект управления игровой сессией");
            }

            Transform body = transform.Find("body");
            if (body == null)
                Debug.LogError("Отсутствует тело персонажа " + transform.name);
            else
            {
                _animator = body.GetComponent<Animator>();
                if (_animator == null)
                    Debug.LogError("Отсутствует компонент Animator у тела " + transform.name);
            }

            if (GameSession != null)
                GameSession.RegisterCharacter(this);

            //HACK: если будет несколько потомков с таким компонентом, возможно некорректное поведение. Реализация только в рамках тестового задания.
            _spriteRender = transform.GetComponentInChildren<SpriteRenderer>();
            if (_spriteRender == null)
                Debug.LogError("Отсутствует SpriteRenderer у потомков " + transform.name);
        }

        void Start()
        {
            Health = StartHealth;
        }

        void Update()
        {
            // если мёртвые - ничего не делаем
            if (_dead)
            {
                if (MyQueue)
                {
                    StopAllCoroutines();
                    StartCoroutine(NextQueue());
                }
                return;
            }
            // плавное перемещение в нужную позицию.
            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * SpeedMovement);
            
            // опредеяем направления жеста и перемещаем персонажа в ту клетку, что там находится, если это возможно
            Cell nextCell = GetCellDirection(DirectionMovement);
            if (nextCell != null && MyQueue)
            {
                transform.localScale = DirectionMovement == ESwipeDirection.Left ? new Vector3(-1, 1, 1) : Vector3.one;
                switch (CanMove(nextCell))
                {
                    case EMoveType.Attack:
                        Character enemy = GetCharacterByCell(nextCell);
                        DoAttack(enemy);
                        break;
                    case EMoveType.Move:
                        DoMove(nextCell);
                        break;
                    case EMoveType.Finish:
                        DoMove(nextCell);
                        DoFinish();
                        break;
                    default:
                        break;
                }
                StopAllCoroutines();
                StartCoroutine(NextQueue());
            }
        }

        void FixedUpdate()
        {
            // выравниваем сортировку спрайта по инвертированной позиции Y.
            //HACK: 1208 - значение позиции по Y самой верхей доступной клетки + 2 (клетки игрового поля и точки окончания уровня)
            if (_spriteRender != null && !_dead)
                _spriteRender.sortingOrder = 1210 - (int)transform.position.y;
        }

        /// <summary>
        /// Метод осуществляет проверку возможности перемещения на указанную клетку и определяет тип данного перемещения.
        /// </summary>
        /// <param name="cell">Клетка на которую необходимо переместиться.</param>
        /// <returns>Тип перемещения.</returns>
        public EMoveType CanMove(Cell cell)
        {
            if (cell == null)
                return EMoveType.Not;

            if (cell.transform.childCount == 0 && !cell.Blocked)
                return EMoveType.Move;
            
            //HACK: Здесь идёт проверка только на первого потомка в структуре, при развитии проекта необходимо
            // изменить подобный подход. Но в данный момент реализования общего анализа будет лишней.
            switch (cell.transform.GetChild(0).tag)
            {
                case "enemy":
                    return gameObject.tag == "enemy" ? EMoveType.Not : EMoveType.Attack;
                case "finish":
                    return EMoveType.Finish;
                case "cell":
                    return EMoveType.Move;
                case "hero":
                    return EMoveType.Attack;
                default:
                    return EMoveType.Not;
            }
        }

        /// <summary>
        /// Метод возвращает базовый класс персонажа ячейки. Если в ячейке находится другой объект или она пустая возвращается null.
        /// </summary>
        /// <param name="cell">Ячейка, противника которой необходимо вернуть.</param>
        /// <returns>Возвращает компонент персонажа.</returns>
        protected Character GetCharacterByCell(Cell cell)
        {
            if (cell.transform.childCount > 0)
            {
                //HACK: Здесь отсутствует проверка на наличия нескольких потомков у ячейки.
                // При развитии проекта необходимо данное поле проверить.
                Transform character = cell.transform.GetChild(0);
                //if (character.tag == "hero" || character.tag == "enemy")
                    return character.GetComponent<Character>();
            }
            return null;
        }

        /// <summary>
        /// Определяет клетку по направлению движения.
        /// </summary>
        /// <param name="dir">Тип направления движения.</param>
        /// <returns>Возвращает ближайшую клетку в этом направлении.</returns>
        protected Cell GetCellDirection(ESwipeDirection dir)
        {
            Vector2 cellPos = new Vector2(_parent.transform.position.x, _parent.transform.position.y);
            Vector2 pos = Vector2.zero;
            switch (dir)
            {
                case ESwipeDirection.Up:
                    pos = Vector2.up;
                    break;
                case ESwipeDirection.Down:
                    pos = Vector2.down;
                    break;
                case ESwipeDirection.Right:
                    pos = Vector2.right;
                    break;
                case ESwipeDirection.Left:
                    pos = Vector2.left;
                    break;
                default:
                    return null;
            }
            pos = cellPos + new Vector2(_parent.Size.x * pos.x, _parent.Size.y * pos.y);
            //Debug.Log(pos);
            return GetCell(pos);
        }

        /// <summary>
        /// Метод определяет есть ли возможность двигаться в указанном направлении и возвращает тип этого движения.
        /// </summary>
        /// <param name="direction">Направление движения.</param>
        /// <returns>Возвращает тип движения персонажа.</returns>
        public EMoveType CanMoveToDirection(ESwipeDirection direction)
        {
            return CanMove(GetCellDirection(direction));
        }

        /// <summary>
        /// Возвращает клетку, которая имеет указанную позицию.
        /// </summary>
        /// <param name="position">Позиция клетки, которую необходимо получить.</param>
        /// <returns>Клетка с данной позицией.</returns>
        protected Cell GetCell(Vector3 position)
        {
            if (_grid == null)
                return null;

            return _grid.Cells.ContainsKey(position) ? _grid.Cells[position] : null;
        }

        /// <summary>
        /// Метод обеспечивает получения родительской клетки, в которой находится персонаж.
        /// </summary>
        protected void GetParentCell()
        {
            _parent = transform.parent.GetComponent<Cell>();
        }

        /// <summary>
        /// Метод осуществляет перемещение персонажа на необходимую ячейку.
        /// </summary>
        /// <param name="nextCell">Ячейка на которую необходимо переместиться.</param>
        private void DoMove(Cell nextCell)
        {
            if (_animator != null)
                _animator.SetTrigger(CharAnim.Movement);
            transform.parent = nextCell.transform;
            GetParentCell();
        }

        /// <summary>
        /// Метод реализует атаку персонажа.
        /// </summary>
        /// <param name="enemy">Другой персонаж, которого необходимо атаковать.</param>
        public void DoAttack(Character enemy)
        {
            if (_animator != null && !_animator.GetBool(CharAnim.Moving))
                _animator.SetTrigger(CharAnim.Attack);
            if (enemy != null)
            {
                enemy.DoHurt();
                enemy.Health -= AttackForce;
                Health -= enemy.AttackForce;
            }
        }

        /// <summary>
        /// Метод реализует получение повреждений.
        /// </summary>
        public void DoHurt()
        {
            if (_animator != null)
                _animator.SetTrigger(CharAnim.Hurt);
        }

        /// <summary>
        /// Метод реализует умирание персонажа.
        /// </summary>
        public void DoDie()
        {
            if (_animator != null)
                _animator.SetTrigger(CharAnim.Die);
            _dead = true;
            _deadParent = transform.parent;
            _spriteRender.sortingOrder = 2;
            transform.parent = null;
            if (gameObject.tag == "hero")
                GameSession.StateSession = EStateSession.Failure;
        }

        /// <summary>
        /// Метод реализует победу персонажа. Объекты с тегом enemy не учитываются.
        /// </summary>
        public void DoFinish()
        {
            if (gameObject.tag == "enemy")
                return;
            if (GameSession != null)
            {
                MyQueue = false;
                DirectionMovement = ESwipeDirection.None;
                GameSession.StateSession = EStateSession.Finish;
            }
        }

        /// <summary>
        /// Метод реализует переход права хода следующему персонажу.
        /// </summary>
        /// <returns></returns>
        public IEnumerator NextQueue()
        {
            DirectionMovement = ESwipeDirection.None;
            MyQueue = false;
            // в задании не было указано, но я сделал с небольшой задержкой смену очереди, чтобы игрок мог отфиксировать изменения.
            yield return new WaitForSeconds(0.5f);
            if (gameObject.tag == "hero")
                GameSession.NextState();
            else
                GameSession.NextCharacter();
        }
    }
}