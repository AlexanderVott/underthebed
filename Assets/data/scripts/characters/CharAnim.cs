﻿using UnityEngine;

namespace RedDev.Gameplay.Characters
{
    /// <summary>
    /// Статичный класс, хранящий хеши названий анимаций персонажей.
    /// </summary>
    public static class CharAnim
    {
        /// <summary>
        /// Тригер перемещения.
        /// </summary>
        public static int Movement = Animator.StringToHash("Movement");

        /// <summary>
        /// Булевая переменная. True когда проигрывается анимация движения.
        /// </summary>
        public static int Moving = Animator.StringToHash("Moving");

        /// <summary>
        /// Тригер атаки.
        /// </summary>
        public static int Attack = Animator.StringToHash("Attack");

        /// <summary>
        /// Тригер проигрывания анимации повреждения.
        /// </summary>
        public static int Hurt = Animator.StringToHash("Hurt");

        /// <summary>
        /// Тригер проигрывания анимации смерти.
        /// </summary>
        public static int Die = Animator.StringToHash("Die");
    }
}