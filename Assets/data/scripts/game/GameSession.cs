﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using RedDev.Gameplay.Characters;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RedDev.Gameplay.Game
{
    /// <summary>
    /// Тип описывает состояние игровой сессии (ход игрока, или противника).
    /// </summary>
    public enum EStateSession
    {
        /// <summary>
        /// Ход игрока.
        /// </summary>
        Player = 0,

        /// <summary>
        /// Ход противников.
        /// </summary>
        Enemies,

        /// <summary>
        /// Пауза. В данной сборке не используется.
        /// </summary>
        Pause,

        /// <summary>
        /// Проигрышь.
        /// </summary>
        Failure,

        /// <summary>
        /// Удачное завершение уровня, победа.
        /// </summary>
        Finish
    }

    /// <summary>
    /// Класс описывает игровые сессии.
    /// </summary>
    public class GameSession : MonoBehaviour
    {
        /// <summary>
        /// Статичное поле, которое хранит загруженные бандлы для работы с переходами между уровнями.
        /// </summary>
        public static Dictionary<string, AssetBundle> AssetBundles = new Dictionary<string, AssetBundle>();

        /// <summary>
        /// Делегат описывает вызов события изменения состояни игровой сессии.
        /// </summary>
        /// <param name="newState">Новое состояние, на которое происходит переключение.</param>
        /// <param name="oldState">Прошлое состояние, с которого происходит переключение.</param>
        public delegate void ChangeStateSession(EStateSession newState, EStateSession oldState);

        /// <summary>
        /// Событие вызывается при изменении игровой сесcии.
        /// </summary>
        public event ChangeStateSession onChangeStateSession;

        private EStateSession _stateSession;

        /// <summary>
        /// Состояние игровой сессии.
        /// </summary>
        public EStateSession StateSession
        {
            get { return _stateSession; }
            set
            {
                if (onChangeStateSession != null)
                    onChangeStateSession(value, _stateSession);
                _stateSession = value;
            }
        }

        /// <summary>
        /// Список персонажей участвующих в игровой сессии.
        /// </summary>
        public readonly List<Character> Characters = new List<Character>();
        private int _currentEnemy = 0;

        /// <summary>
        /// Ссылка на персонажа игрока.
        /// </summary>
        public Character Hero { get; private set; }
        private bool _errorNoHero = false;
        
        /// <summary>
        /// Делегат описывает вызов события вызова игровой подсказки.
        /// </summary>
        /// <param name="textHint"></param>
        public delegate void GameHintState(string textHint);

        /// <summary>
        /// Событие вызывается при изменении состояния игровой сессии и отображении игровой подсказки.
        /// </summary>
        public event GameHintState onGameHintState;

        void Start()
        {
#if UNITY_ANDROID
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
#endif
            StateSession = EStateSession.Enemies;
            NextState();
        }

        void Update()
        {
            if (!_errorNoHero)
            {
                if (Hero == null) 
                    Debug.LogError("Не был зарегистрирован ни один герой игровой сессии");
                _errorNoHero = true;
            }

            /*if (Input.GetButtonDown("Cancel"))
                StateSession = EStateSession.Pause;*/
        }
        
        /// <summary>
        /// Регистрирует в объекте игровой сессии персонажа.
        /// Если персонаж имеет отличительные признаки героя, то регистрируется как герой.
        /// Всем персонажам назначаются соответствующие теги.
        /// </summary>
        /// <param name="character">Персонаж, которого необходимо зарегистрировать.</param>
        public void RegisterCharacter(Character character)
        {
            if (character.GetComponent<CharacterController>() != null)
                RegisterHero(character);
            else
            {
                Characters.Add(character);
                character.gameObject.tag = "enemy";
            }
        }

        /// <summary>
        /// Регистрация персонажа под управлением игрока.
        /// </summary>
        /// <param name="hero">Персонажа, которого необходимо зарегистрировать.</param>
        public void RegisterHero(Character hero)
        {
            if ((hero != null) && (Hero != null) && (hero.gameObject.GetInstanceID() != Hero.gameObject.GetInstanceID()))
            {
                Debug.LogError("Регистрируется несколько персонажей под управлением игрока: " + hero.gameObject.name + " и " + Hero.gameObject.name);
                return;
            }
            Hero = hero;
            Hero.gameObject.tag = "hero";
        }

        /// <summary>
        /// Переключает очередь ходов противников.
        /// </summary>
        public void NextCharacter()
        {
            _currentEnemy++;
            Debug.Log("Next character: " + _currentEnemy);
            if (_currentEnemy >= Characters.Count)
            {
                _currentEnemy = 0;
                Debug.Log("Next state");
                NextState();
            }
            else
            {
                Characters[_currentEnemy].MyQueue = true;
            }
        }

        /// <summary>
        /// Переключает общее игровое состояние.
        /// </summary>
        public void NextState()
        {
            string textHint = String.Empty;
            if (StateSession == EStateSession.Player)
            {
                StateSession = EStateSession.Enemies;
                textHint = "ход противника";
                if (Characters.Count > 0)
                {
                    _currentEnemy = 0;
                    Characters[_currentEnemy].MyQueue = true;
                }
            }
            else if (StateSession == EStateSession.Enemies)
            {
                StateSession = EStateSession.Player;
                textHint = "ход игрока";
            }
            if (onGameHintState != null)
                onGameHintState(textHint);
        }

        /// <summary>
        /// Возвращает путь до следующего по порядку игрового уровня.
        /// </summary>
        /// <returns>Если ничего не найден будет возвращен null.</returns>
        public string GetNextLevel()
        {
            string pattern = @"(?<path>.*)(?<num>\d+)";
            var match = Regex.Match(SceneManager.GetActiveScene().path, pattern);

            int nextId = Convert.ToInt32(match.Groups["num"].Value) + 1;
            string path = match.Groups["path"].Value + nextId;
            return (GameSession.AssetBundles.ContainsKey("scenes") && GameSession.AssetBundles["scenes"].Contains("level" + nextId)) ? path : null;
        }
    }
}