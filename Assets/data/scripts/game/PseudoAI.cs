﻿using System;
using RedDev.Gameplay.Characters;
using RedDev.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RedDev.Gameplay.Game
{
    /// <summary>
    /// Класс описывает псевдологику ИИ для противника.
    /// </summary>
    [RequireComponent(typeof(Character))]
    public class PseudoAI : MonoBehaviour
    {
        private Character _character;
        private GameSession _gameSession;

        void Start()
        {
            _character = GetComponent<Character>();
            _gameSession = Transform.FindObjectOfType<GameSession>();
            if (_gameSession == null)
                Debug.LogError("Неудалось найти объект сессии для " + transform.name);
        }
        
        void Update()
        {
            if (_character.GameSession.StateSession != EStateSession.Enemies)
                return;

            _character.DirectionMovement = _character.MyQueue ? Logic() : ESwipeDirection.None;
        }

        void FixedUpdate()
        {
        }

        /// <summary>
        /// Основной метод, описывающий логику противника.
        /// </summary>
        /// <returns></returns>
        private ESwipeDirection Logic()
        {
            // если в игровой сессии не назначен персонаж игрока - ничего не делаем
            if (_gameSession.Hero == null)
                return ESwipeDirection.None;

            int resultRand = -1;
            ESwipeDirection result = ESwipeDirection.None;
            const float tolerance = 0.001f;

            // реализовал самую примитивную логику поведения противника, для разнообразия действий присутствует random.
            // вечного цикла быть не должно, но counter нас обезопасит
            int counter = 0;
            while (_character.CanMoveToDirection(result) == EMoveType.Not)
            {
                if (counter > 8)
                    return (ESwipeDirection)Random.Range(1, 5);
                counter++;
                if (Math.Abs(_gameSession.Hero.transform.position.x - transform.position.x) > tolerance &&
                    Math.Abs(_gameSession.Hero.transform.position.y - transform.position.y) > tolerance)
                    resultRand = Random.Range(0, 2);

                if (Math.Abs(_gameSession.Hero.transform.position.x - transform.position.x) < tolerance ||
                    resultRand == 0)
                    result = _gameSession.Hero.transform.position.y > transform.position.y
                        ? ESwipeDirection.Up
                        : ESwipeDirection.Down;
                else if (Math.Abs(_gameSession.Hero.transform.position.y - transform.position.y) < tolerance ||
                         resultRand == 1)
                    result = _gameSession.Hero.transform.position.x > transform.position.x
                        ? ESwipeDirection.Right
                        : ESwipeDirection.Left;
            }

            return result;
        }
    }
}