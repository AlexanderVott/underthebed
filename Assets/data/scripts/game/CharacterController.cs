﻿using RedDev.Gameplay.Characters;
using RedDev.Utils;
using UnityEngine;

namespace RedDev.Gameplay.Game
{
    /// <summary>
    /// Класс описывает управление персонажа игроком.
    /// </summary>
    [RequireComponent(typeof(Character))]
    public class CharacterController : MonoBehaviour
    {
        private Character _character;
        private bool _blockingControl = false;

        void Start()
        {
            _character = GetComponent<Character>();
        }
        
        void Update()
        {
            if (_character.GameSession.StateSession != EStateSession.Player)
            {
                _blockingControl = false;
                return;
            }

            ESwipeDirection swipeDir = SwipeDetector.Instance.SwipeDirection;
            if (!_character.MyQueue &&
                !_blockingControl &&
                swipeDir != ESwipeDirection.None && 
                _character.CanMoveToDirection(swipeDir) != EMoveType.Not)
            {
                _character.MyQueue = true;
                _character.DirectionMovement = swipeDir;
                _blockingControl = true;
            }
        }

        void FixedUpdate()
        {
        }
    }
}