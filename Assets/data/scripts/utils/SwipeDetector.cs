﻿using Free.Utils;
using UnityEngine;

namespace RedDev.Utils
{
    /// <summary>
    /// Тип описывает направление жеста.
    /// Можно расширить вертикальными значениями.
    /// </summary>
    public enum ESwipeDirection
    {
        None = 0,
        Up,
        Down,
        Left,
        Right
    }

    /// <summary>
    /// Класс-синглтон для реализиации управления жестами игроком.
    /// </summary>
    public class SwipeDetector : Singleton<SwipeDetector>
    {
        /// <summary>
        /// Минимальная длина жеста, позволяющая отсеивать простые нажатия.
        /// </summary>
        [Tooltip("Минимальная длина жеста")]
        public float MinLengthGesture = 100.0f;

        private Vector2 _beginPos;
        private Vector2 _endPos;
        private Vector2 _currentSwipe;

        /// <summary>
        /// Направление жеста.
        /// </summary>
        public ESwipeDirection SwipeDirection { get; private set; }

        void Start()
        {
            SwipeDirection = ESwipeDirection.None;
        }

        void Update()
        {
            DetectDirection();
        }

        /// <summary>
        /// Метод обеспечивает работу с косаниями или мышью для реализации жестов.
        /// </summary>
        private void DetectDirection()
        {
            SwipeDirection = ESwipeDirection.None;
            if (Input.touches.Length > 0)
            {
                Touch t = Input.GetTouch(0);

                if (t.phase == TouchPhase.Began)
                    BeginEvent(new Vector2(t.position.x, t.position.y));

                if (t.phase == TouchPhase.Ended)
                    SwipeDirection = EndEvent(new Vector2(t.position.x, t.position.y));
            }
            else
            {
                if (Input.GetMouseButtonDown(0))   
                    BeginEvent(new Vector2(Input.mousePosition.x, Input.mousePosition.y));

                if (Input.GetMouseButtonUp(0))
                    SwipeDirection = EndEvent(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            }
        }

        /// <summary>
        /// Метод-событие реализует сохраненеи позиции в начале выполнения жеста.
        /// </summary>
        /// <param name="pos"></param>
        private void BeginEvent(Vector2 pos)
        {
            _beginPos = pos;
        }

        /// <summary>
        /// Метод-событие реализует функционал определения направления при завершении жеста.
        /// </summary>
        /// <param name="pos">Позиция точки жеста при его завершении.</param>
        /// <returns>Возвращает направление жеста.</returns>
        private ESwipeDirection EndEvent(Vector2 pos)
        {
            _endPos = pos;
            _currentSwipe = new Vector2(_endPos.x - _beginPos.x, _endPos.y - _beginPos.y);

            // убеждаемся в том, что это именно жест, а не нажатие
            if (_currentSwipe.magnitude <= MinLengthGesture)
                return ESwipeDirection.None;

            // для определения вертикальных значений лучше использовать больше сравнений с использованием скалярного произведения векторов
            if (Mathf.Abs(_currentSwipe.x) > Mathf.Abs(_currentSwipe.y))
                return _currentSwipe.x < 0.0f ? ESwipeDirection.Left : ESwipeDirection.Right;
            else
                return _currentSwipe.y < 0.0f ? ESwipeDirection.Down : ESwipeDirection.Up;
        }
    }
}