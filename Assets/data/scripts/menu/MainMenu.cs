﻿using System;
using System.Collections;
using RedDev.Gameplay.Game;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private Button _btn;

    // Ссылка на объект прогрессбара
    [Tooltip("Ссылка на объект прогресс бара.")]
    public Image ProgressBar;
    
	void Start ()
	{
	    _btn = GetComponent<Button>();
	    if (_btn == null)
	        Debug.LogError("Отсутствует компонент Button на кнопке " + transform.name);
	    else
	    {
	        _btn.onClick.AddListener(new UnityAction(() =>
	        {
	            if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCount)
	                StartCoroutine(Loading());
	        }));
	    }

        if (ProgressBar == null)
            Debug.LogError("ProgressBar не был задан для " + transform.name);
	}

    void Update()
    {
        if (ProgressBar != null && ProgressBar.gameObject.activeSelf)
            ProgressBar.transform.Rotate(Vector3.forward, -5.0f);
    }

    /// <summary>
    /// Метод вызывается при старте загрузки уровня.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Loading()
    {
        ProgressBar.gameObject.SetActive(true);

        yield return null;
        StartCoroutine(LoadingBundle());
    }

    /// <summary>
    /// Загрузка бандлов.
    /// </summary>
    /// <returns></returns>
    private IEnumerator LoadingBundle()
    {
        string mainBundle = "StreamingAssets";
#if UNITY_ANDROID
        string path = "https://alexandervott.github.io/underthebed/AndroidAssetBundles/";
#elif UNITY_IOS
        string path = "https://alexandervott.github.io/underthebed/IOSAssetBundles/";
        mainBundle = "IOSAssetBundles";
#elif UNITY_WEBGL
        string path = "https://alexandervott.github.io/underthebed/StreamingAssets/";
#else
        string path = "file:///" + Application.streamingAssetsPath + "/";
#endif

        string[] bundles;
        AssetBundleManifest manifest;
        
        using (WWW wwwBundles = new WWW(path + mainBundle))
        {
            yield return wwwBundles;

            if (!String.IsNullOrEmpty(wwwBundles.error))
            {
                Debug.LogError("Ошибка получения манифеста Bundles: " + wwwBundles.error);
                yield break;
            }
            manifest = wwwBundles.assetBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            bundles = manifest.GetAllAssetBundles();
        }
        
        GameSession.AssetBundles.Clear();
        for (int i = 0; i < bundles.Length; i++)
        {
            using (
                WWW wwwBundle = WWW.LoadFromCacheOrDownload(path + bundles[i], manifest.GetAssetBundleHash(bundles[i])))
            {
                yield return wwwBundle;

                if (!String.IsNullOrEmpty(wwwBundle.error))
                {
                    Debug.LogError("Ошибка загрузки bundle: " + wwwBundle.error);
                    yield break;
                }
                GameSession.AssetBundles.Add(bundles[i], wwwBundle.assetBundle);
                Debug.Log(bundles[i] + " is loaded");
            }
        }

        SceneManager.LoadSceneAsync(GameSession.AssetBundles["scenes"].GetAllScenePaths()[0]);
    }
}
