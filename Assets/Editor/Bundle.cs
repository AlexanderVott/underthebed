﻿using UnityEditor;

namespace RedDev.Editor.Bundles
{
    public class Bundle
    {
        /// <summary>
        /// Метод осуществляет сборку внешних ресурсов в указанную директорию.
        /// </summary>
        [MenuItem("RedDev/Build bundles")]
        public static void BuildBundles()
        {
            string path = EditorUtility.SaveFolderPanel("Save bundle", "", "");
            if (path.Length > 0)
            {
                BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ForceRebuildAssetBundle,
                    EditorUserBuildSettings.activeBuildTarget);
            }
        }
    }
}