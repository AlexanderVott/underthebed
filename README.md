# Тестовое задание

У героя и противников есть два параметра: количество жизней и урон. Значения можете выбрать произвольные, лишь бы играть было возможно. Количество жизней героя нужно выводить на экран.

Герой и противники ходят по очереди. Первым ходит герой.

Игрок может, при помощи горизонтальных и вертикальных жестов на мобильном устройстве и с помощью мыши в браузере, перемещать героя по горизонтали и по вертикали на одну ячейку.

Если ячейка свободна — герой переходит на неё.

Если в ячейке выход — уровень успешно завершен; противник — герой атакует.

В результате атаки количество жизней противника уменьшается на величину урона героя. Если количество жизней противника уменьшилось до нуля, он считается погибшим и больше не ходит, а его ячейка считается свободной. После атаки герой возвращается в свою ячейку, не зависимо от результата атаки.
Затем по очереди ходят противники. Противники должны приближаться к герою, но не использовать алгоритмов поиска пути, учитывающих обход препятствий.
Если на момент своего хода, противник находится в соседней от героя ячейке, он тратит свой ход на атаку, и количество жизней героя уменьшается на величину урона противника.
Если жизней у героя не осталось, он считается погибшим, и игра заканчивается. Об этом игроку сообщает заставка.

При нажатии на кнопку «Заново» игра запускается заново.
Если герой достиг выхода — победа! Об этом игроку сообщает заставка:

При нажатии на кнопку «Заново» игра запускается заново.

## Технические требования

### Мобильная версия

Предпочтительнее будет создание приложения для iOS-устройств.
В этом случае результатом выполнения задания должно быть приложение в виде .ipa файла, которое мы сможем протестировать на наших устройствах. Приложение должно корректно работать на смартфонах не младше iPhone 5 и на планшетах не младше iPad AIR 2 под управлением операционной системы не младше 8-й версии. Достаточно реализовать поддержку работы только при ландшафтной ориентации устройства.
Возможно создание приложения для Android-устройств. Достаточно поддержки операционной системы начиная с версии 4.4.

### Версия для браузера

Игра должна быть внедрена в веб-страницу. Размеры игры 800 на 600 пикселей. Вы можете опубликовать приложение в интернете и прислать нам ссылку на страницу с игрой, если есть такая возможность, либо прислать нам папку с файлом index.html и всем необходимым для работы приложения.

### Загрузка графики

При запуске приложения в браузере должен визуализироваться процесс загрузки. Достаточно использовать средства, которые предоставляет Unity.
При этом должны загружаться только те данные, которые необходимы для показа стартового экрана. Остальные ресурсы, используемые в игровом процессе, должны начать загружаться после показа стартового экрана.

### Сборки
[Скачать apk](https://bitbucket.org/AlexanderVott/underthebed/downloads/UnderTheBed.apk)

[WebGL](https://vk.com/app6060899_135356031)